# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class AfyaData(models.Model):
	fname = models.CharField(null=False, max_length=15)
	lname = models.CharField(null=False, max_length=15)
	age = models.PositiveIntegerField(null=False) 
	sex = models.CharField(null=False, max_length=15) 
	glucose = models.DecimalField(null=False, decimal_places=2, max_digits=6, default=0.00)
	smoker = models.CharField(null=False, max_length=15)
	diabetes = models.CharField(null=False, max_length=15)
	phone_no = models.CharField(null=True, max_length=15)
	sbp = models.PositiveIntegerField(null=False)
	color = models.CharField(null=False, max_length=15)
	datecreated = models.DateTimeField(null=False)
	reporter = models.CharField(null=True, max_length=20)
	active = models.BooleanField(default=True)

class AfricaIsTalkingLog(models.Model):
	msg_id = models.IntegerField(null=False) 
	raw_msg = models.CharField(null=False, max_length=1000)
	link_id = models.CharField(null=True, max_length=1000)
	msg_to = models.CharField(null=False, max_length=1000)
	msg_from = models.CharField(null=False, max_length=1000)
	text = models.CharField(null=False, max_length=1000)
	date_sent = models.DateTimeField(null=False, max_length=1000)
	active = models.BooleanField(default=True, max_length=1000)

class ErrorLog(models.Model):
	incomingmsg = models.CharField(null=False, max_length=1000)
	reporter = models.CharField(null=True, max_length=20)
	errormsg = models.CharField(null=False, max_length=1000)
	datecreated = models.DateTimeField(null=False)