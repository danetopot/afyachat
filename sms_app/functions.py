# functions.py
from django.db.models import Count
import datetime
import psycopg2
import csv
import sys
import os
import stat
import shutil
import random
from models import AfyaData

def generate_dashboard():
	print 'Preparing the afyachat dashboard . . .'
	try:
		dash = {}

		data = AfyaData.objects.filter(active=True)
		total = data.count()
		male = data.exclude(sex='F').count()
		female = data.exclude(sex='M').count()
		smoker = data.filter(smoker='YES').count()
		diabetes = data.filter(diabetes='YES').count()
		results = ''
		if data:
			results = data.values('color').annotate(
				unit_count=Count('color')).order_by('-unit_count')			

		dash['total'] = total
		dash['male'] = male
		dash['female'] = female
		dash['smoker'] = smoker
		dash['diabetes'] = diabetes
		dash['results'] = results
	except Exception:
		print 'Error preparing the hiv dashboard !!'
		pass
	else:
		return dash


# Generate CSV
def generate_csv():
	print 'generate_csv process... '

	dateOfToday = datetime.datetime.today()
	randomnumber = random.randint(1000, 9999)
	filename = "afyachat_data_" + str(dateOfToday) + \
		"_" + str(randomnumber) + ".csv"
	try:
		# Write to hard drive
		terminator = '"'
		outfile = "/home/administrator/Desktop/DATA/" + filename
		sql = "COPY (SELECT * FROM afyachat_afyadata) TO '" + \
			outfile + "'With  DELIMITER ',' CSV HEADER"
		conn = psycopg2.connect(database="afyachat", user="postgres",
								password="postgres", host="127.0.0.1", port="5432")
		print 'Connection to database success [generate_csv] . . '
		cursor = conn.cursor()
		cursor.execute(sql)
		cursor.close()

	except Exception, e:
		print 'Error in generate_csv .. %s' % str(e)
	else:
		pass
	finally:
		pass
	return True

def save_africaistalking(message):
	print(message)
	return