# works with both python 2 and 3
from __future__ import print_function
import africastalking
from dbcontext import DbContext
from processmessages import Process


class SMS:
	def __init__(self):
		# Set your app credentials
		self.username = "devdannette"
		self.api_key = "10c1220e66f8aa54b142fce6796571188599c07f6a0e5044b3d00f8801c9abe0"
		# Initialize the SDK
		africastalking.initialize(self.username, self.api_key)
		# Get the SMS service
		self.sms = africastalking.SMS

	def fetch_sms_sync(self):
		# Our API will return 100 messages at a time back to you, starting with what you currently believe is the lastReceivedId.
		# Specify 0 for the first time you access the method and the ID of the last message we sent you on subsequent calls

		#last_received_id = 0
		last_received_id = DbContext().get_last_received_id()
		print('last_received_id : %s' %last_received_id)
		try:
			hasinbox = False
			# Fetch all messages using a loop
			while True:
				MessageData = self.sms.fetch_messages(last_received_id)
				messages = MessageData['SMSMessageData']['Messages']
				if len(messages) == 0:
					print('No New SMS Msgs in AfricaIsTalking Cloud Storage!')
					self.read_sms_sync();
					break
				else:
					hasinbox = True
					for message in messages:
						DbContext().save_message(message)

						# Reassign the lastReceivedId
						last_received_id = int(message['id'])
						#print(last_received_id)
						# NOTE: Be sure to save the lastReceivedId for next time
			return hasinbox
		except Exception as e:
			print('Encountered an error while fetching: %s' % str(e))

	def read_sms_sync(self):
		conn = DbContext().connect()
		try:
			print('Reading SMS from PostgreSQL Local Storage .. ')

			cur = conn.cursor()
			sql = ("""
				SELECT msg_id, text, msg_from FROM sms_app_africaistalkinglog WHERE active=True
			""")

			cur.execute(sql)
			while True:
				row = cur.fetchone()
				if row == None:
					break
				else:
					msgid = row[0]
					msg = row[1]
					msgfrom = row[2]
					Process().process_sms_sync(msgid, msg.lower(), msgfrom)
			cur.close()

		except Exception as e:
			print('Encountered an error while fetching: %s' % str(e))
