# works with both python 2 and 3
from __future__ import print_function
import datetime
import psycopg2
import logging
# from datetime import datetime
from dateutil.parser import parse

# Setup Logging
filename = 'C:\\Users\\Danet Opot\\Desktop\\PROJECTS\\afyachat_uganda\\sms_app\\logs\\africaistalking.log'
# filename= '/home/administrator/Desktop/AFYACHAT_UGANDA/hiv_project/afyachat/africaistalking_fetchsms.log'
logging.basicConfig(filename=filename, level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(name)s %(message)s')
logger = logging.getLogger(__name__)


class DbContext:
    def __init__(self):
        self.database = "afyachat"
        self.username = "postgres"
        self.password = "postgres"

    def connect(self):
        logger.info('Connecting To Database...')
        conn = psycopg2.connect(host="localhost",
                                database=self.database,
                                user=self.username,
                                password=self.password)
        return conn

    def save_afyadata(self, fname, lname, age, sex, smoker, diabetes, glucose, sbp, phone, msgfrom, outputMsg, msgid):
        try:
            today = datetime.datetime.now()
            conn = self.connect()
            cur = conn.cursor()

            fname = "'" + fname.upper() + "'"
            lname = "'" + lname.upper() + "'"
            age = age
            sex = "'" + sex + "'"
            glucose = glucose
            smoker = "'" + smoker + "'"
            diabetes = "'" + diabetes + "'"
            phone = "'" + phone + "'"
            sbp = sbp
            color = "'" + outputMsg + "'"
            datecreated = "'" + str(today) + "'"
            reporter = "'" + msgfrom + "'"

            sqlinsert = ("""
				INSERT INTO sms_app_afyadata(fname, lname, age, sex, glucose, smoker, diabetes, phone_no, 
			sbp, color, datecreated, reporter, active)
				VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
				""") % (fname.upper(), lname.upper(), age, sex, glucose, smoker, diabetes, phone, sbp, color, datecreated, reporter, True)
            logger.info(sqlinsert)

            cur.execute(sqlinsert)
            conn.commit()

            sqlupdate = ("""
				UPDATE sms_app_africaistalkinglog SET active=%s WHERE msg_id=%s
				""") % (False, msgid)
            cur.execute(sqlupdate)
            conn.commit()

            cur.close()
            conn.close()

        except Exception as e:
            logger.error('Error Saving To AfyaData :- %s' % str(e))

    def save_message(self, message):
        try:
            logger.info('Saving Msg To Database...%s' % message)
            conn = self.connect()
            cur = conn.cursor()

            msg_id = int(message['id'])
            link_id = "'" + message['linkId'] + "'"
            msg_to = "'" + message['to'] + "'"
            msg_from = "'" + message['from'] + "'"
            text = "'" + message['text'] + "'"
            date_sent = "'" + str(parse(message['date'])) + "'"
            raw_msg = (str(msg_id) + "|"
                       + str(message['linkId']) + "|"
                       + message['to'] + "|"
                       + message['from'] + "|"
                       + message['text'] + "|"
                       + str(parse(message['date'])))
            raw_msg = "'" + raw_msg + "'"
            active = True

            sql = ("""
				INSERT INTO sms_app_africaistalkinglog(msg_id, raw_msg, link_id, msg_to, msg_from,text, date_sent, active)
				VALUES(%d,%s,%s,%s,%s,%s,%s,%s)
				""") % (msg_id, raw_msg, link_id, msg_to, msg_from, text, date_sent, active)

            cur.execute(sql)
            conn.commit()
            cur.close()
            conn.close()
        except Exception as e:
            logger.error('Error Saving To AfricaIsTalkingLog :- %s' % str(e))

    def save_error(self, message, messagefrom, messageerror):
        try:
            logger.info('Saving To ErrorLog...')
            conn = self.connect()
            cur = conn.cursor()

            msglist = ' '.join(map(str, message))
            message = "'" + msglist + "'"
            messagefrom = "'" + messagefrom + "'"
            messageerror = "'" + messageerror.strip() + "'"
            datecreated = "'" + str(datetime.datetime.now()) + "'"
            sql = ("""
				INSERT INTO sms_app_errorlog(incomingmsg, reporter, errormsg,datecreated)
				VALUES(%s,%s,%s,%s)
				""") % (message, messagefrom, messageerror, datecreated)
            logger.info(sql)
            cur.execute(sql)
            conn.commit()
            cur.close()
            conn.close()
        except Exception as e:
            logger.error('Error Saving To ErrorLog :- %s' % str(e))

    def update_status(self, msgid):
        try:
            logger.info('Updating AfricaIsTalkingLog...')
            conn = self.connect()
            cur = conn.cursor()
            sql = ("""
				UPDATE sms_app_africaistalkinglog SET active=%s WHERE msg_id=%s
				""") % (False, msgid)
            logger.info(sql)
            cur.execute(sql)
            conn.commit()
            cur.close()
            conn.close()
        except Exception as e:
            logger.error('Error Updating AfricaIsTalkingLog :- %s' % str(e))

    def get_last_received_id(self):
        last_received_id = 0
        try:
            logger.info('Getting Last Received ID...')
            conn = self.connect()
            cur = conn.cursor()
            sql = "select msg_id from sms_app_africaistalkinglog order by date_sent desc limit 1"
            cur.execute(sql)
            while True:
                row = cur.fetchone()
                if row == None:
                    break
                last_received_id = row[0]
        except Exception as e:
            logger.error('Error Getting Last Received ID :- %s' % str(e))
        return last_received_id
