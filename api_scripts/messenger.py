#!/usr/bin/python
# -*- coding: utf-8 -*-
import requests
import json
import datetime
import logging
from fetchmessages import SMS

# Setup Logging
filename = 'C:\\Users\\Danet Opot\\Desktop\\PROJECTS\\afyachat_uganda\\sms_app\\logs\\africaistalking.log'
# filename= '/home/administrator/Desktop/AFYACHAT_UGANDA/hiv_project/afyachat/africaistalking_fetchsms.log'
logging.basicConfig(filename=filename, level=logging.INFO, format='%(asctime)s %(levelname)s %(name)s %(message)s')
logger=logging.getLogger(__name__)

try:
	logger.info('Starting To Fetch SMS...')
	fetch = SMS().fetch_sms_sync()

	#if fetch:
	#	SMS().read_sms_sync()
except Exception as e:
	logger.error(str(e))
