# works with both python 2 and 3
from __future__ import print_function
import re
import psycopg2
import logging
import africastalking
import datetime
from dateutil.parser import parse
from dbcontext import DbContext

# Setup Logging
filename = 'C:\\Users\\Danet Opot\\Desktop\\PROJECTS\\afyachat_uganda\\sms_app\\logs\\africaistalking.log'
# filename= '/home/administrator/Desktop/AFYACHAT_UGANDA/hiv_project/afyachat/africaistalking_fetchsms.log'
logging.basicConfig(filename=filename, level=logging.INFO,
					format='%(asctime)s %(levelname)s %(name)s %(message)s')
logger = logging.getLogger(__name__)


class Process:
	def __init__(self):
																																																																																																																																																																																																	# Set your app credentials
		self.username = "devdannette"
		self.api_key = "10c1220e66f8aa54b142fce6796571188599c07f6a0e5044b3d00f8801c9abe0"
		# Initialize the SDK
		africastalking.initialize(self.username, self.api_key)
		# Get the SMS service
		self.sms = africastalking.SMS

	def send_sms_sync(self, msg, msgfrom):
		try:
			username = "devdannette"
			api_key = "10c1220e66f8aa54b142fce6796571188599c07f6a0e5044b3d00f8801c9abe0"
			africastalking.initialize(self.username, self.api_key)
			sms = africastalking.SMS

			recepients = []
			recepients.append(msgfrom)
			response = sms.send(msg, recepients)
		except Exception as e:
			print('Encountered an error while processing: %s' % str(e))

	def process_sms_sync(self, msgid, msg, msgfrom):
		try:
			msgout,fname,lname,age,sex,smoker,glucose,sbp,phone,datecreated = None,None,None,None,None,None,None,None,None,None
			if msg.startswith('afya'):
				msg = msg.split()[1:]

				if(len(msg) > 0):
					# Generate Help Message
					if(msg[0].lower() == "help"):
						msgout = "Send a message using the format AFYA Firstname Lastname Age Sex Smoker Glucose SystolicPressure e.g. AFYA John Doe 75 M No 10.0 130 0700000000"
						DbContext().save_error(msg, msgfrom, msgout)
						DbContext().update_status(msgid)
						self.send_sms_sync(msgout, msgfrom)
					else:
						if(len(msg) == 8):
							fname = msg[0]
							lname = msg[1]
							age = msg[2]
							sex = msg[3]
							smoker = msg[4]
							glucose = msg[5]
							sbp = msg[6]
							phone = msg[7]
							datecreated = datetime.datetime.now()

							if not fname:
								msgout = "Sorry,please enter a valid message or missing firstname and resend e.g. AFYA John Doe 75 M No 10.0 130 0700000000"
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)
							if not re.match("^[A-Za-z]", fname):
								msgout = "Sorry,invalid firstname.Please enter a valid firstname using only alphabets and resend."
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)

							if not lname:
								msgout = "Sorry,please enter a valid message or missing lastname and resend e.g. AFYA John Doe 75 M No 10.0 130 0700000000."
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)
							if not re.match("^[A-Za-z]", lname):
								msgout = "Sorry,invalid lastname.Please enter a valid lastname using only alphabets and resend."
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)

							if not age:
								msgout = "Sorry,please enter a valid message or missing age and resend e.g. AFYA John Doe 75 M No 10.0 130 0700000000."
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)
							if not age.isdigit():
								msgout = "Sorry,please enter a valid age and resend.Make sure you enter numbers only for the age."
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)
							if int(age) <= 0:
								msgout = "Sorry,age cannot be ZERO or less than ZERO,please enter a valid age and resend."
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)
							if int(age) < 40:
								msgout = "Thank you,your SMS has been received.Unfortunately age less than 40 isn't factored in the WHO Chart."
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)

							sex = sex.upper()
							if not sex:
								msgout = "Sorry,please enter a valid message or missing gender and resend e.g. AFYA John Doe 75 M No 10.0 130 0700000000.Use M or F for male and female respectively."
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)
							if not re.match("^[A-Za-z]", sex):
								msgout = "Sorry,invalid gender.Please enter a valid gender using only alphabets and resend."
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)
							if not ((sex == "M") or (sex == "F")):
								msgout = "Sorry,invalid gender.If Male type M, if Female type F without quotes and resend."
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)

							smoker = smoker.upper()
							if not smoker:
								msgout = "Sorry,please enter a valid message or missing smoking response and resend e.g. AFYA John Doe 75 M No 10.0 130 0700000000.If smoker type Yes, if non-smoker type No."
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)
							if not re.match("^[A-Za-z]", smoker):
								msgout = "Sorry,invalid smoking response.Please enter a valid smoking response using only alphabets and resend."
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)
							if not ((smoker == "YES") or (smoker == "NO")):
								msgout = "Sorry,invalid smoking response.If smoker type Yes, if non-smoker type No."
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)

							if not float(glucose):
								msgout = "Sorry,please enter a valid message or missing blood glucose in mmol/L and resend e.g. AFYA John Doe 75 M No 10.0 130 0700000000."
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)

							if not re.match("^\d{0,2}(?:\.\d)?$", glucose):
								msgout = "Sorry,invalid blood glucose in mmol/L response.Please enter a valid blood glucose response using only digits and an optional one decimal point and resend."
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)

							diabetes_float = 0.0
							diabetes_float = float(glucose)
							diabetes = 'YES' if diabetes_float > 11.0 else 'NO'
							if diabetes_float < 0.0 or diabetes_float > 100.0:
								msgout = "Sorry,invalid blood glucose in mmol/L response.Please enter a blood glucose between 0.0 and 99.9 inclusive, and resend."
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)

							if not sbp:
								msgout = "Sorry,please enter a valid message or missing systolic pressure and resend e.g. AFYA John Doe 75 M No 10.0 130 0700000000."
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)
							if not sbp.isdigit():
								msgout = "Sorry,please enter a valid systolic pressure and resend.Make sure you enter numbers only for the systolic pressure."
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)
							if int(sbp) <= 0:
								msgout = "Sorry,systolic pressure cannot be ZERO or less than ZERO,please enter a valid systolic pressure and resend."
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)

							if not phone:
								msgout = "Sorry,please enter a valid message or missing phone number and resend e.g. AFYA John Doe 75 M No 10.0 130 0700000000."
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)
							if not phone.isdigit():
								msgout = "Sorry,please enter a valid phone number using only digits and resend."
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)
							if not (len(phone) == 10):
								msgout = "Sorry,please enter a valid phone number.Please check if the number of digits in the phone number is 10."
								DbContext().save_error(msg, msgfrom, msgout)
								DbContext().update_status(msgid)
								self.send_sms_sync(msgout, msgfrom)

							print("Proceeding to processing . . .")
							# Send SMS
							outputName = fname + ' ' + lname
							outputMsg = self.process_incoming_message(age, sex, smoker, diabetes, sbp)
							response = 'NAME: ' + outputName.upper() + ' \nCOLOR: ' + outputMsg
							DbContext().save_afyadata(fname, lname, age, sex, smoker,
									  diabetes, glucose, sbp, phone, msgfrom, outputMsg, msgid)
							self.send_sms_sync(response, msgfrom)
						else:
							msgout = ("""The message you entered does not match the required format.Please send AFYA HELP to get help.""")
							DbContext().save_error(msg, msgfrom, msgout)
							DbContext().update_status(msgid)
							self.send_sms_sync(msgout, msgfrom)
				else:
					msgout = ("""The message you entered does not match the required format.Please send AFYA HELP to get help.""")
					DbContext().save_error(msg, msgfrom, msgout)
					DbContext().update_status(msgid)
					self.send_sms_sync(msgout, msgfrom)

		except Exception as e:
			print('Encountered an error while processing: %s' % str(e))

	def process_incoming_message(self, age, sex, smoker, diabetes, sbp):
		# Initialize output message
		outputMsg = "Thank you,your SMS has been received."

		# Casting the variables to their types
		age = int(age)
		sex = str(sex.upper())
		smoker = str(smoker.upper())
		diabetes = str(diabetes.upper())
		sbp = int(sbp)

		# Class of >= 70
		if(age >= 70):
			if(sex == "M"):
				if(smoker == "YES"):
					if(diabetes == "YES"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "YELLOW"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "ORANGE"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "RED"
						if(sbp >= 180):
							outputMsg = "CRIMSON"
					if(diabetes == "NO"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "YELLOW"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "YELLOW"
						if(sbp >= 180):
							outputMsg = "RED"

				elif(smoker == "NO"):
					if(diabetes == "NO"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "YELLOW"
						if(sbp >= 180):
							outputMsg = "ORANGE"
					if(diabetes == "YES"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "YELLOW"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "ORANGE"
						if(sbp >= 180):
							outputMsg = "CRIMSON"

			elif (sex == 'F'):
				if(smoker == "YES"):
					if(diabetes == "YES"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "YELLOW"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "ORANGE"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "RED"
						if(sbp >= 180):
							outputMsg = "CRIMSON"
					if(diabetes == "NO"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "YELLOW"
						if(sbp >= 180):
							outputMsg = "RED"

				elif(smoker == "NO"):
					if(diabetes == "NO"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "GREEN"
						if(sbp >= 180):
							outputMsg = "ORANGE"
					if(diabetes == "YES"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "ORANGE"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "YELLOW"
						if(sbp >= 180):
							outputMsg = "CRIMSON"

		# Class of 60-69
		if(age >= 60 and age <= 69):
			if(sex == "M"):
				if(smoker == "YES"):
					if(diabetes == "YES"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "YELLOW"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "ORANGE"
						if(sbp >= 180):
							outputMsg = "CRIMSON"
					if(diabetes == "NO"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "YELLOW"
						if(sbp >= 180):
							outputMsg = "RED"

				elif(smoker == "NO"):
					if(diabetes == "NO"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "GREEN"
						if(sbp >= 180):
							outputMsg = "ORANGE"
					if(diabetes == "YES"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "YELLOW"
						if(sbp >= 180):
							outputMsg = "CRIMSON"

			elif (sex == 'F'):
				if(smoker == "YES"):
					if(diabetes == "YES"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "YELLOW"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "ORANGE"
						if(sbp >= 180):
							outputMsg = "CRIMSON"
					if(diabetes == "NO"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "YELLOW"
						if(sbp >= 180):
							outputMsg = "RED"

				elif(smoker == "NO"):
					if(diabetes == "NO"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "GREEN"
						if(sbp >= 180):
							outputMsg = "ORANGE"
					if(diabetes == "YES"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "YELLOW"
						if(sbp >= 180):
							outputMsg = "RED"

		# Class of 50-59
		if(age >= 50 and age <= 59):
			if(sex == "M"):
				if(smoker == "YES"):
					if(diabetes == "YES"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "ORANGE"
						if(sbp >= 180):
							outputMsg = "CRIMSON"
					if(diabetes == "NO"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "YELLOW"
						if(sbp >= 180):
							outputMsg = "RED"

				elif(smoker == "NO"):
					if(diabetes == "NO"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "YELLOW"
						if(sbp >= 180):
							outputMsg = "RED"
					if(diabetes == "YES"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "GREEN"
						if(sbp >= 180):
							outputMsg = "RED"

			elif (sex == 'F'):
				if(smoker == "YES"):
					if(diabetes == "YES"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "YELLOW"
						if(sbp >= 180):
							outputMsg = "CRIMSON"
					if(diabetes == "NO"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "GREEN"
						if(sbp >= 180):
							outputMsg = "RED"

				elif(smoker == "NO"):
					if(diabetes == "NO"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "GREEN"
						if(sbp >= 180):
							outputMsg = "YELLOW"
					if(diabetes == "YES"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "GREEN"
						if(sbp >= 180):
							outputMsg = "RED"

		# Class of 40-49
		if(age >= 40 and age <= 49):
			if(sex == "M"):
				if(smoker == "YES"):
					if(diabetes == "YES"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "ORANGE"
						if(sbp >= 180):
							outputMsg = "CRIMSON"
					if(diabetes == "NO"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "YELLOW"
						if(sbp >= 180):
							outputMsg = "RED"

				elif(smoker == "NO"):
					if(diabetes == "NO"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "GREEN"
						if(sbp >= 180):
							outputMsg = "YELLOW"
					if(diabetes == "YES"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "GREEN"
						if(sbp >= 180):
							outputMsg = "RED"

			elif (sex == 'F'):
				if(smoker == "YES"):
					if(diabetes == "YES"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "GREEN"
						if(sbp >= 180):
							outputMsg = "ORANGE"
					if(diabetes == "NO"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "GREEN"
						if(sbp >= 180):
							outputMsg = "ORANGE"

				elif(smoker == "NO"):
					if(diabetes == "NO"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "GREEN"
						if(sbp >= 180):
							outputMsg = "YELLOW"
					if(diabetes == "YES"):
						if(sbp > 0 and sbp <= 139):
							outputMsg = "GREEN"
						if(sbp >= 140 and sbp <= 159):
							outputMsg = "GREEN"
						if(sbp >= 160 and sbp <= 179):
							outputMsg = "GREEN"
						if(sbp >= 180):
							outputMsg = "ORANGE"

		# return output message
		return outputMsg
