var handleBootstrapWizardsValidation = function() {
    "use strict";
    $("#mysubmit").removeClass( "btn-primary" ).attr("disabled", "disabled");
    $("#wizard").bwizard({ validating: function (e, ui) { 
            if (ui.index == 0) {
                // step-1 validation
                $(".alert").hide();
                if (false === $('form[name="form-wizard"]').parsley().validate('primary')) {
                    $(".alert").show();
                    $('.invalid-form-message').html('Please provide details about the organisation.');
                    return false;
                }
            } else if (ui.index == 1) {
                // step-2 validation
                $(".alert").hide();
                  if (false === $('form[name="form-wizard"]').parsley().validate('primary1')) {
                    $(".alert").show();
                    $('.invalid-form-message').html('Please provide organisation type details');
                    return false;
                }
            } else if (ui.index == 2) {
                // step-3 validation
                $(".alert").hide();
                if (false === $('form[name="form-wizard"]').parsley().validate('primary2')) {
                    $(".alert").show();
                    $('.invalid-form-message').html('Please provide required location details');
                    return false;
                }else{
                    $("#mysubmit").addClass( "btn-primary" ).removeAttr("disabled");
                }
            } else if (ui.index == 3) {
                // step-4 validation
                $(".alert").hide();
                $("#mysubmit").addClass( "btn-primary" ).removeAttr("disabled");
                if (false === $('form[name="form-wizard"]').parsley().validate('primary3')) {
                    $(".alert").show();
                    $('.invalid-form-message').html('Please provide required contact details');
                    return false;
                }
            }
        } 
    });

    $("#wizard2").bwizard({ validating: function (e, ui) { 
            if (ui.index == 0) {
                // step-1 validation
                if (false === $('form[name="form-wizard2"]').parsley().validate('group0')) {
                    return false;
                }
            } else if (ui.index == 1) {
                // step-2 validation
                
                $("#mysubmit2").addClass( "btn-primary" ).removeAttr("disabled");
                  if (false === $('form[name="form-wizard2"]').parsley().validate('group1')) {
                    return false;
                }
            }else if (ui.index == 2) {
                // step-3 validation
                 
                  if (false === $('form[name="form-wizard2"]').parsley().validate('group2')) {
                    return false;
                }
            } 
        } 
    });

    $("#wizard3").bwizard({ validating: function (e, ui) { 
            if (ui.index == 0) {
                // step-1 validation
                if (false === $('form[name="form-wizard3"]').parsley().validate('group0')) {
                    return false;
                }
            } else if (ui.index == 1) {
                // step-2 validation
                $("#mysubmit3").addClass( "btn-primary" ).removeAttr("disabled");
                if (false === $('form[name="form-wizard3"]').parsley().validate('group1')) {
                    return false;
                }
            }else if (ui.index == 2) {
                // step-3 validation
                 
                  if (false === $('form[name="form-wizard3"]').parsley().validate('group2')) {
                    return false;
                }
            } 
      }            
    });
};

var FormWizardValidation = function () {
    "use strict";
    return {
        //main function
        init: function () {
            handleBootstrapWizardsValidation();
        }
    };
}();